﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationEmpleados.Entidades;

namespace WebApplicationEmpleados.Funcionalidades
{
    public class EmpleadoBL
    {
        public static List<EmpleadoBE> GetEmpleados()
        {
            List<EmpleadoBE> empleado = new List<EmpleadoBE>();
            empleado.Add(new EmpleadoBE() { dni = "73353800", nombre = "Janice", apellido = "Rafael", puesto = "001" });
            empleado.Add(new EmpleadoBE() { dni = "70123919", nombre = "Gaby", apellido = "Ramirez", puesto = "002" });
            empleado.Add(new EmpleadoBE() { dni = "74071977", nombre = "Christian", apellido = "Quispe", puesto = "003" });
            empleado.Add(new EmpleadoBE() { dni = "47162219", nombre = "Oscar", apellido = "Perez", puesto = "001" });
            empleado.Add(new EmpleadoBE() { dni = "42080891", nombre = "Victor", apellido = "Peña", puesto = "002" });
            empleado.Add(new EmpleadoBE() { dni = "70019174", nombre = "Josué", apellido = "Quispe", puesto = "003" });
            return empleado;
        }

        public static List<PuestoBE> GetSueldo()
        {
            List<PuestoBE> sueldo = new List<PuestoBE>();
            sueldo.Add(new PuestoBE() { codigo = "001", puesto = "Gerente", sueldo = "10000" });
            sueldo.Add(new PuestoBE() { codigo = "002", puesto = "Sub Gerente", sueldo = "7000" });
            sueldo.Add(new PuestoBE() { codigo = "003", puesto = "Comercial", sueldo = "5000" });
            return sueldo;
        }

        public static PuestoBE GetPagoPlanilla(string dni)
        {
            var listEmpleados = GetEmpleados();
            var empleadobl = listEmpleados.FirstOrDefault(x => x.dni == dni);

            var puesto = new PuestoBE();
            if (empleadobl != null)
            {
                var sueldos = GetSueldo();
                puesto = sueldos.FirstOrDefault(x => x.codigo == empleadobl.puesto);
            }

            return puesto;
        }

        public static EmpleadoBE GetEmpleado(string dni)
        {
            var listEmpleados = GetEmpleados();
            var emp = listEmpleados.FirstOrDefault(x => x.dni == dni);
            return emp;
        }
    }
}