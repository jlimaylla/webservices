﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplicationEmpleados.Entidades;

namespace WebApplicationEmpleados
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonConsultar_Click(object sender, EventArgs e)
        {
            ServiceReference1.WebService1SoapClient ws = new ServiceReference1.WebService1SoapClient();
            string result = ws.ConsultarEmpleado(TextBoxDni.Text);
            if (result == null)
            {
                LabelResultado.Text ="Empleado No Encontrado";
            }
            else
            {
                LabelResultado.Text = result;
            }

        }

    }
}