﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConsultaEmpleado.aspx.cs" Inherits="WebApplicationEmpleados.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lbl1" runat="server" Text="CONSULTA EMPLEADO" ></asp:Label>
            <br />
            <asp:Label ID="dni" runat="server" Text="DNI"></asp:Label>
            &nbsp;
            <asp:TextBox ID="TextBoxDni" runat="server" Height="18px" Width="102px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="ButtonConsultar" runat="server" Text="Consultar" OnClick="ButtonConsultar_Click" />
            <br />
            <br />
            <asp:Label ID="LabelResultado" runat="server" Text=""></asp:Label>
            <br />
        </div>
    </form>
</body>
</html>
