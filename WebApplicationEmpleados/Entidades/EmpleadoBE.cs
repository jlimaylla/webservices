﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationEmpleados.Entidades
{
    public class EmpleadoBE
    {
        public string dni { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string puesto { get; set; }
        public string otro { get; set; }
    }
}