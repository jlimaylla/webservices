﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationEmpleados.Entidades
{
    public class PuestoBE
    {
        public string codigo { get; set; }
        public string puesto { get; set; }
        public string sueldo { get; set; }
    }
}