﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using WebApplicationEmpleados.Entidades;
using WebApplicationEmpleados.Funcionalidades;

namespace WebApplicationEmpleados
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {
        /// <summary>
        /// Metodo Web que lista
        /// todos los empleados de planilla
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<EmpleadoBE> ListarEmpleados()
        {
            return EmpleadoBL.GetEmpleados();
        }

        /// <summary>
        /// Metodo Web para consultar si el numero de dni
        ///  ingresado es de un empleado de planilla
        /// </summary>
        /// <param name="dni"></param>
        /// <returns></returns>
        [WebMethod]
        public EmpleadoBE ConsultarEmpleado(string dni)
        {
            return EmpleadoBL.GetEmpleado(dni);
        }

        /// <summary>
        /// Metodo Web para determinar el pago total 
        /// de planilla de empleados
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public PuestoBE PagoPlanilla(string dni)
        {
            return EmpleadoBL.GetPagoPlanilla(dni);
        }
    }
}
